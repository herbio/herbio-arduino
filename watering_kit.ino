#include <DallasTemperature.h>
#include <ArduinoJson.h>
#include <OneWire.h>
#include "RTClib.h"
RTC_DS1307 RTC;

/* Set all PIN IDs */
// set all moisture sensors PIN ID
int moisture1 = A0;
int moisture2 = A1;
int moisture3 = A2;
int moisture4 = A3;
// set uv sensor PIN ID
int uvSenzorPin = A4;
// set all ultrasonic sensors PIN ID
int echoPin = 3; // SCL
int trigPin = 2; // SDA
// set temperature sensor PIN ID
int tempSenzorPin = 7;
// set water relays PIN IDs
int relay1 = 6;
int relay2 = 8;
int relay3 = 9;
int relay4 = 10;
// set water pump PIN ID
int pump = 4;
// set button PIN ID
int button = 12;

/* Setup variables for storing sensor values and states */
// set global volues for ultrasonic sensor
long duration;
int distance;
// Temperature sensor
OneWire oneWire(tempSenzorPin);
DallasTemperature sensors(&oneWire);
// UV sensor
float uvSensorVoltage;
float uvSensorValue;
// Temperature Sensor
String temp;

// Moisture tresholds
int moisture_1_Up_Treshold = 50;
int moisture_1_Down_Treshold = 30;
int moisture_2_Up_Treshold = 50;
int moisture_2_Down_Treshold = 30;
int moisture_3_Up_Treshold = 50;
int moisture_3_Down_Treshold = 30;
int moisture_4_Up_Treshold = 50;
int moisture_4_Down_Treshold = 30;
// Moisture values
int moisture1_value = 0;
int moisture2_value = 0;
int moisture3_value = 0;
int moisture4_value = 0;
// Watering modes 0: MANUAL 1: AUTO
int mode1 = 1;
int mode2 = 1;
int mode3 = 1;
int mode4 = 1;
// watering mode manual water amount in ml
int manual_water_amount1 = 20;
int manual_water_amount2 = 20;
int manual_water_amount3 = 20;
int manual_water_amount4 = 20;
// state flags   1:open   0:close
int pump_state_flag = 0;
int relay1_state_flag = 0;
int relay2_state_flag = 0;
int relay3_state_flag = 0;
int relay4_state_flag = 0;
// time of 1ml
int oneMl = 60;

// Auto watering - temp vars
int temp_plantAutoModeChangeId;
int temp_plantAutoMoistureThresholdUp;
int temp_plantAutoMoistureThresholdDown;
// Manual watering - temp vars
int temp_plantManualId;
int temp_plantManualModeWateringAmount;

void setup()
{
  delay(2000);
  sensors.begin();
  // setup pins
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  Serial.begin(9600);
  // declare relay as output
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);
  pinMode(relay4, OUTPUT);
  // declare pump as output
  pinMode(pump, OUTPUT);
  // declare switch as input
  pinMode(button, INPUT);
}

void loop()
{
  CheckSerialSwitchCase();

  measureMoistureSensors();
  measureUV();
  measureTemp();
  measureDistance();
  waterAllFlowers();
}

void CheckSerialSwitchCase()
{
  if (Serial.available())
  {

    char commandcharacter = Serial.read();

    switch (commandcharacter)
    {

    case 'a': // auto watering mode - a 1 [0..100] [0..100]

      while (!Serial.available())
        ;

      temp_plantAutoModeChangeId = Serial.parseInt();
      temp_plantAutoMoistureThresholdUp = Serial.parseInt();
      temp_plantAutoMoistureThresholdDown = Serial.parseInt();

      // Serial.print("Message was: ");
      Serial.println(temp_plantAutoModeChangeId);
      Serial.println(temp_plantAutoMoistureThresholdUp);
      Serial.println(temp_plantAutoMoistureThresholdDown);

      // Set manual watering mode vars
      changeWateringModeToAuto(temp_plantAutoModeChangeId, temp_plantAutoMoistureThresholdUp, temp_plantAutoMoistureThresholdDown);

      break;

    case 'm': // m = "manual"; m 1 50

      temp_plantManualId = Serial.parseInt();
      temp_plantManualModeWateringAmount = Serial.parseInt();
      Serial.print(temp_plantManualId);
      Serial.print(temp_plantManualModeWateringAmount);

      changeWateringModeToManual(temp_plantManualId, temp_plantManualModeWateringAmount);

      break;

    case 'w': // a = "water flower manual"; w 1
      while (!Serial.available())
        ;

      temp_plantManualId = Serial.parseInt();
      waterFlowerManualById(temp_plantManualId);

      Serial.println(temp_plantManualId);
      break;

    case 'j':
      sendRoomState();
    }
  }
}

/* Mesauring from sensors */

void measureDistance(void)
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034 / 2;
}

void measureUV(void)
{
  uvSensorValue = analogRead(A4);
  uvSensorVoltage = uvSensorValue / 1024 * 5.0;
}

void measureTemp(void)
{
  sensors.requestTemperatures();
  temp = sensors.getTempCByIndex(0);
}

// Get moisture sensors values
void measureMoistureSensors()
{
  float value1 = analogRead(A0);
  moisture1_value = map(value1, 600, 360, 0, 100);
  delay(20);
  if (moisture1_value < 0)
  {
    moisture1_value = 0;
  }

  float value2 = analogRead(A1);
  moisture2_value = map(value2, 600, 360, 0, 100);
  delay(20);
  if (moisture2_value < 0)
  {
    moisture2_value = 0;
  }

  float value3 = analogRead(A2);
  moisture3_value = map(value3, 600, 360, 0, 100);
  delay(20);
  if (moisture3_value < 0)
  {
    moisture3_value = 0;
  }

  float value4 = analogRead(A3);
  moisture4_value = map(value4, 600, 360, 0, 100);
  delay(20);
  if (moisture4_value < 0)
  {
    moisture4_value = 0;
  }
}

void sendRoomState()
{
  StaticJsonDocument<192> doc; // setup document for json
  doc["temp"] = temp;
  doc["distance"] = distance;
  doc["lighting"] = uvSensorValue;
  doc["moisture1"] = moisture1_value;
  doc["moisture2"] = moisture2_value;
  doc["moisture3"] = moisture3_value;
  doc["moisture4"] = moisture4_value;
  doc["pumpState"] = pump_state_flag;
  doc["ventilState1"] = relay1_state_flag;
  doc["ventilState2"] = relay2_state_flag;
  doc["ventilState3"] = relay3_state_flag;
  doc["ventilState4"] = relay4_state_flag;
  doc["threshold1U"] = moisture_1_Up_Treshold;
  doc["threshold2U"] = moisture_2_Up_Treshold;
  doc["threshold3U"] = moisture_3_Up_Treshold;
  doc["threshold4U"] = moisture_4_Up_Treshold;
  doc["threshold1D"] = moisture_1_Down_Treshold;
  doc["threshold2D"] = moisture_2_Down_Treshold;
  doc["threshold3D"] = moisture_3_Down_Treshold;
  doc["threshold4D"] = moisture_4_Down_Treshold;
  serializeJson(doc, Serial); // send json
  Serial.println();
}

void waterAllFlowers()
{

  if (mode1 == 1)
  {
    if (moisture1_value < moisture_1_Down_Treshold)
    {
      digitalWrite(relay1, HIGH);
      relay1_state_flag = 1;
      delay(50);
      if (pump_state_flag == 0)
      {
        digitalWrite(pump, HIGH);
        pump_state_flag = 1;
        delay(50);
      }
    }
    else if (moisture1_value > moisture_1_Up_Treshold)
    {
      digitalWrite(relay1, LOW);
      relay1_state_flag = 0;
      delay(50);
      if ((relay1_state_flag == 0) && (relay2_state_flag == 0) && (relay3_state_flag == 0) && (relay4_state_flag == 0))
      {
        digitalWrite(pump, LOW);
        pump_state_flag = 0;
        delay(50);
      }
    }
  }
  if (mode2 == 1)
  {
    if (moisture2_value < moisture_2_Down_Treshold)
    {
      digitalWrite(relay2, HIGH);
      relay2_state_flag = 1;
      delay(50);
      if (pump_state_flag == 0)
      {
        digitalWrite(pump, HIGH);
        pump_state_flag = 1;
        delay(50);
      }
    }
    else if (moisture2_value > moisture_2_Up_Treshold)
    {
      digitalWrite(relay2, LOW);
      relay2_state_flag = 0;
      delay(50);
      if ((relay1_state_flag == 0) && (relay2_state_flag == 0) && (relay3_state_flag == 0) && (relay4_state_flag == 0))
      {
        digitalWrite(pump, LOW);
        pump_state_flag = 0;
        delay(50);
      }
    }
  }
  if (mode3 == 1)
  {
    if (moisture3_value < moisture_3_Down_Treshold)
    {
      digitalWrite(relay3, HIGH);
      relay3_state_flag = 1;
      delay(50);
      if (pump_state_flag == 0)
      {
        digitalWrite(pump, HIGH);
        pump_state_flag = 1;
        delay(50);
      }
    }
    else if (moisture3_value > moisture_3_Up_Treshold)
    {
      digitalWrite(relay3, LOW);
      relay3_state_flag = 0;
      delay(50);
      if ((relay1_state_flag == 0) && (relay2_state_flag == 0) && (relay3_state_flag == 0) && (relay4_state_flag == 0))
      {
        digitalWrite(pump, LOW);
        pump_state_flag = 0;
        delay(50);
      }
    }
  }
  if (mode4 == 1)
  {
    if (moisture4_value < moisture_4_Down_Treshold)
    {
      digitalWrite(relay4, HIGH);
      relay4_state_flag = 1;
      delay(50);
      if (pump_state_flag == 0)
      {
        digitalWrite(pump, HIGH);
        pump_state_flag = 1;
        delay(50);
      }
    }
    else if (moisture4_value > moisture_4_Up_Treshold)
    {
      digitalWrite(relay4, LOW);
      relay4_state_flag = 0;
      delay(50);
      if ((relay1_state_flag == 0) && (relay2_state_flag == 0) && (relay3_state_flag == 0) && (relay4_state_flag == 0))
      {
        digitalWrite(pump, LOW);
        pump_state_flag = 0;
        delay(50);
      }
    }
  }
}

void waterFlowerManualById(int id)
{
  if (id == 1)
  {
    waterFlowerManual(relay1, relay1_state_flag, manual_water_amount1);
  }
  else if (id == 2)
  {
    waterFlowerManual(relay2, relay2_state_flag, manual_water_amount2);
  }
  else if (id == 3)
  {
    waterFlowerManual(relay3, relay3_state_flag, manual_water_amount3);
  }
  else if (id == 4)
  {
    waterFlowerManual(relay4, relay4_state_flag, manual_water_amount4);
  }
}

void waterFlowerManual(int relay, int relayStateFlag, int manualWaterAmount)
{
  digitalWrite(relay, HIGH);
  relayStateFlag = 1;
  delay(50);
  if (pump_state_flag == 0)
  {
    digitalWrite(pump, HIGH);
    pump_state_flag = 1;
    delay(50);
  }

  delay(manualWaterAmount * oneMl);
  digitalWrite(relay, LOW);
  relayStateFlag = 0;
  delay(50);
  if ((relay1_state_flag == 0) && (relay2_state_flag == 0) && (relay3_state_flag == 0) && (relay4_state_flag == 0))
  {
    digitalWrite(pump, LOW);
    pump_state_flag = 0;
    delay(50);
  }
}

void changeWateringModeToAuto(int id, int plantThresholdUp, int plantThresholdDown)
{
  if (id == 1)
  {
    moisture_1_Up_Treshold = plantThresholdUp;
    moisture_1_Down_Treshold = plantThresholdDown;
    mode1 = 1;
  }
  else if (id == 2)
  {
    moisture_2_Up_Treshold = plantThresholdUp;
    moisture_2_Down_Treshold = plantThresholdDown;
    mode2 = 1;
  }
  else if (id == 3)
  {
    moisture_3_Up_Treshold = plantThresholdUp;
    moisture_3_Down_Treshold = plantThresholdDown;
    mode3 = 1;
  }
  else if (id == 4)
  {
    moisture_4_Up_Treshold = plantThresholdUp;
    moisture_4_Down_Treshold = plantThresholdDown;
    mode4 = 1;
  }
}

void changeWateringModeToManual(int id, int wateringAmount)
{
  if (id == 1)
  {
    manual_water_amount1 = wateringAmount;
    mode1 = 0;
  }
  else if (id == 2)
  {
    manual_water_amount2 = wateringAmount;
    mode2 = 0;
  }
  else if (id == 3)
  {
    manual_water_amount3 = wateringAmount;
    mode3 = 0;
  }
  else if (id == 4)
  {
    manual_water_amount4 = wateringAmount;
    mode4 = 0;
  }
}
